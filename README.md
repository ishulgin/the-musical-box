# The Musical Box

## Build Instructions

Follow these steps:

* Install [Haskell Platform](https://www.haskell.org/platform/).
    * Download and install [GHC](https://www.haskell.org/ghc/download.html). 
    * Download and install [Stack](https://docs.haskellstack.org/en/stable/README/#how-to-install). 
    * Download and install [Cabal](https://www.haskell.org/cabal/download.html).

* Install and configure [PostgreSQL](https://www.postgresql.org/download/).
* Add directory with `pg_config` to `PATH` environment variable.
* Execute `db.sql` script.
* Set `TMB_DB` environment variable according to the format:
    
    ```
    host='HOST' port=PORT dbname='DBNAME' user='USER' password='PASS'
    ```
* Install [Chromaprint](https://acoustid.org/chromaprint).
* Add directory with `fpcalc` to `PATH` environment variable.
* Register application at [Acoustid](https://acoustid.org/my-applications) website. 
* Add API Key of your application to `ACOUSTID_TOKEN` environment variable.
* Register your [Telegram-bot](https://core.telegram.org/bots#6-botfather) following Telegram's instructions.
* Add bot's api key to `TMB_BOT` environment variable.
* Install [ngrok](https://ngrok.com/) or any other similar tool.
* Set `TMB_PORT` environment variable.
* Start ngrok with command:
    
    ```
    ngrok http $TMB_PORT
    ```
* Set a Telegram's webhook:

    ```
    curl "https://api.telegram.org/bot${TMB_BOT}/setWebhook?url=https://<ngork_id>.ngrok.io/webhook/bot${TMB_BOT}"
    ```
* Build and run program with Stack:
    ```
    stack build --exec the-musical-box-exe
    ```