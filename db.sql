CREATE TABLE IF NOT EXISTS track (
  id     SERIAL PRIMARY KEY,
  title   VARCHAR NOT NULL,
  album  VARCHAR NOT NULL,
  performer VARCHAR NOT NULL,
  file_id VARCHAR NOT NULL UNIQUE,
  UNIQUE (title, album, performer)
);

CREATE TABLE IF NOT EXISTS track_tag (
  id       SERIAL  NOT NULL,
  name     VARCHAR NOT NULL,
  track_id INTEGER REFERENCES track,
  UNIQUE (name, track_id)
);

CREATE EXTENSION IF NOT EXISTS pg_trgm;