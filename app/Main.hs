module Main where

import           Bot.BotController
import           System.Environment (getEnv)

main :: IO ()
main = do
  port <- getEnv "TMB_PORT"
  startApp (read port :: Int)
