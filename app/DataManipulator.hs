{-# LANGUAGE OverloadedStrings #-}

module DataManipulator
  ( TrackMeta(..)
  , getTrackMeta
  , findByAttributes
  , findByTags
  , findRandom
  , findRegex
  , save
  ) where

import           Audio.AcoustidWrapper
import           Audio.ChromaprintWrapper
import           Audio.MusicBrainzWrapper
import           Bot.Parser
import qualified Data.ByteString.Char8      as C8 (pack)
import           Data.Char                  (toLower)
import           Data.List                  (intercalate)
import           Data.Maybe                 (fromJust, fromMaybe)
import           Data.String                (fromString)
import           Data.String.Utils          (strip)
import           Database.PostgreSQL.Simple
import           System.Environment         (getEnv)
import           Text.StringTemplate        (StringTemplate, newSTMP,
                                             setManyAttrib, toString)

-- | Representation of a recording's meta information
data TrackMeta = TrackMeta
  { title     :: String
  , album     :: String
  , performer :: String
  , fileId    :: String
  , tags      :: Maybe [String]
  } deriving (Show)

-- | Returns all information about the recording
-- If there is no such information returns Nothing
getTrackMeta :: String -> String -> IO (Maybe TrackMeta)
getTrackMeta path fileId' = do
  result <- getChromaprint path >>= getTrackData
  case result of
    Nothing -> return Nothing
    Just trackData -> do
      tags' <- getTags $ musicBrainzId trackData
      return $
        Just $ TrackMeta (trackName trackData) (trackAlbum trackData) (trackPerformer trackData) fileId' $ Just tags'

-- | Saves the recording's information to database
save :: TrackMeta -> IO ()
save trackMeta = do
  connectionString <- getEnv "TMB_DB"
  connection <- connectPostgreSQL (C8.pack connectionString)
  let query =
        "INSERT INTO track VALUES (DEFAULT, ?, ?, ?, ?) " ++
        "ON CONFLICT (title, album, performer) DO UPDATE SET file_id = EXCLUDED.file_id "
      tags' =
        intercalate ", " $
        map (\a -> "(DEFAULT, '" ++ a ++ "', (SELECT id FROM track_insert))") $ fromJust $ tags trackMeta
      query' =
        if null tags'
          then query
          else "WITH track_insert AS (" ++
               query ++ "RETURNING id)" ++ "INSERT INTO track_tag VALUES " ++ tags' ++ " ON CONFLICT DO NOTHING"
  execute connection (fromString query') (title trackMeta, album trackMeta, performer trackMeta, fileId trackMeta)
  close connection
  return ()

-- | Finds track's instances in the database by title, album and performer
-- and returns a list that contains those instances
findByAttributes :: String -> String -> String -> IO [TrackMeta]
findByAttributes title album performer = do
  let attributes = [("title", title), ("album", album), ("performer", performer)]
      query =
        newSTMP $
        "SELECT title, album, performer, file_id " ++
        "FROM (SELECT title, album, performer, file_id, " ++
        "similarity(title, '$title$') AS title_dist, " ++
        "similarity(album, '$album$') AS album_dist, " ++
        "similarity(performer, '$performer$') AS performer_dist " ++
        "FROM track) sub " ++
        "WHERE (sub.title_dist > 0.6 OR TRIM('$title$') = '') AND " ++
        "(sub.album_dist > 0.6 OR TRIM('$album$') = '') AND " ++
        "(sub.performer_dist > 0.6 OR TRIM('$performer$') = '') " ++
        "ORDER BY sub.title_dist, sub.album_dist, sub.performer_dist DESC" :: StringTemplate String
  findTrackMeta $ toString $ setManyAttrib attributes query

-- | Finds track's instances in the database by a list of pairs (identifier, value)
-- Possible identifiers:
-- 'a' for album;
-- 't' for title;
-- 'p' for performer;
-- Returns a list that contains those instances
findRegex :: [(Char, String)] -> IO [TrackMeta]
findRegex [] = return []
findRegex list = do
  let list' = map (\(a, b) -> (a, map toLower $ strip b)) list
      albums = filterByIdentifier list' 'a'
      performers = filterByIdentifier list' 'p'
      titles = filterByIdentifier list' 't'
      albums' = groupAttr albums "album"
      performers' = groupAttr performers "performer"
      titles' = groupAttr titles "title"
      attributes = intercalate " AND " $ filter (/= []) [albums', performers', titles']
      query =
        "SELECT title, album, performer, file_id " ++
        "FROM track " ++ "WHERE " ++ attributes ++ " ORDER BY performer, album, title DESC"
  findTrackMeta query

filterByIdentifier :: [(Char, a)] -> Char -> [a]
filterByIdentifier list id = map snd $ filter (\a -> fst a == id) list

groupAttr :: [String] -> String -> String
groupAttr [] _ = ""
groupAttr list attr =
  "(" ++ intercalate " OR " (map (\a -> "LOWER(" ++ attr ++ ") SIMILAR TO '" ++ a ++ "'") list) ++ ")"

-- | Finds track's instances in the database by a list of tags
-- and returns a list that contains those instances
findByTags :: [String] -> IO [TrackMeta]
findByTags tagsList = do
  let tags' = intercalate " OR " $ map (\a -> "similarity(track_tag.name, '" ++ a ++ "') > 0.7") tagsList
      query =
        "SELECT track.title, album, performer, file_id " ++
        "FROM track_tag " ++
        "INNER JOIN track ON (track.id = track_tag.track_id) " ++
        "WHERE " ++
        tags' ++
        " GROUP BY track.id " ++
        "HAVING count(track.id)=" ++ show (length tagsList) ++ " ORDER BY RANDOM() " ++ "LIMIT 5"
  findTrackMeta query

-- | Returns a list of up to 5 random track's instances from the database
findRandom :: IO [TrackMeta]
findRandom = do
  let query = "SELECT title, album, performer, file_id FROM track ORDER BY RANDOM() LIMIT 5"
  findTrackMeta query

-- | Performs a query to the database and parses response
-- Returns a list of tags
findTrackMeta :: String -> IO [TrackMeta]
findTrackMeta query = do
  connectionString <- getEnv "TMB_DB"
  connection <- connectPostgreSQL $ C8.pack connectionString
  tracks <- query_ connection $ fromString query :: IO [(String, String, String, String)]
  close connection
  return $ map (\(a, b, c, d) -> TrackMeta a b c d Nothing) tracks
