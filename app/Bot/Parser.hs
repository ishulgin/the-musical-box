module Bot.Parser
  ( parseExpr
  ) where

import           Data.Functor.Identity (Identity)
import           Text.Parsec
import           Text.Parsec.String    (Parser)
import qualified Text.Parsec.Token     as Token

langDef :: Token.LanguageDef ()
langDef =
  Token.LanguageDef
  { Token.commentStart = ""
  , Token.commentEnd = ""
  , Token.commentLine = ""
  , Token.nestedComments = False
  , Token.identStart = oneOf identifiers
  , Token.identLetter = letter
  , Token.opStart = oneOf ""
  , Token.opLetter = oneOf ""
  , Token.reservedNames = []
  , Token.reservedOpNames = []
  , Token.caseSensitive = True
  }

identifiers :: String
identifiers = ['a', 'p', 't']

lexer :: Token.TokenParser ()
lexer = Token.makeTokenParser langDef

whiteSpace = Token.whiteSpace lexer

identifier = Token.identifier lexer

value = many1 (noneOf ";")

parseExpr :: String -> Either ParseError [(Char, String)]
parseExpr s = parse (many expr) "" s

expr :: Parser (Char, String)
expr = do
  whiteSpace
  ident <- identifier
  if length ident == 1
    then do
--    TODO replace
      whiteSpace
      char '='
      whiteSpace
      val <- value
      whiteSpace
      char ';'
      return (head ident, val)
    else fail "unexpected size of an identifier"
