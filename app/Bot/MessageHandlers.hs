{-# LANGUAGE OverloadedStrings #-}

module Bot.MessageHandlers
  ( helpMessage
  , unknownCommandMessage
  , sizeErrorMessage
  , downloadFailedMessage
  , audioAcceptedMessage
  , noTrackMetaMessage
  , emptyFindQueryMessage
  , emptyQueryMessage
  , noTracksFoundMessage
  , regexHelpMessage
  , tracksFoundMessage
  , invalidTagsQueryMessage
  , emptyCollectionMessage
  ) where

import           Data.Maybe           (fromJust)
import           Data.Monoid          ((<>))
import qualified Data.Text            as T (Text, pack, unlines)
import           Web.Telegram.API.Bot (ChatId, SendMessageRequest,
                                       sendMessageRequest)

helpMessage :: ChatId -> SendMessageRequest
helpMessage chatId =
  sendMessageRequest chatId $
  T.unlines
    [ "/help - show this message"
    , "/find {title},,{album},,{artist} - search tracks by title, name and artist in your collection"
    , "/tags {tag1},,{tag2}... - search tracks by tags"
    , "/random - get up to random 5 tracks from your collection"
    , "/regex {i=regex;} - search tracks using postgres-like regular expressions"
    , "Just send an audio file to add it to your collection"
    , "Maximum file size is 20 MB"
    ]

unknownCommandMessage :: ChatId -> SendMessageRequest
unknownCommandMessage chatId = sendMessageRequest chatId "Unknown command.\nUse /help to get the list of commands."

sizeErrorMessage :: ChatId -> SendMessageRequest
sizeErrorMessage chatId = sendMessageRequest chatId "Size of an audio file can't be more than 20 MB."

downloadFailedMessage :: ChatId -> SendMessageRequest
downloadFailedMessage chatId = sendMessageRequest chatId "Error ocured while downloading the file. Try again later."

audioAcceptedMessage :: ChatId -> String -> String -> String -> SendMessageRequest
audioAcceptedMessage chatId trackName trackAlbum trackArtist =
  sendMessageRequest chatId $
  T.pack $
  "Track \"" ++ trackName ++ "\" from \"" ++ trackAlbum ++ "\" by " ++ trackArtist ++ " was added to your collection."

noTrackMetaMessage :: ChatId -> T.Text -> SendMessageRequest
noTrackMetaMessage chatId title =
  sendMessageRequest chatId $
  "Metadata of \"" <> title <> "\" can't be found.\n" <>
  "Read here https://acoustid.org/webservice how to index the recording."

emptyFindQueryMessage :: ChatId -> SendMessageRequest
emptyFindQueryMessage chatId =
  sendMessageRequest chatId $
  "Use next syntax: \"/find {title},,{album},,{artist}\".\n" <> "At least one of three arguments is required."

emptyQueryMessage :: ChatId -> SendMessageRequest
emptyQueryMessage chatId = sendMessageRequest chatId "Query is empty."

noTracksFoundMessage :: ChatId -> String -> SendMessageRequest
noTracksFoundMessage chatId "" = sendMessageRequest chatId $ T.pack "Nothing was found for your query."
noTracksFoundMessage chatId query =
  sendMessageRequest chatId $ T.pack $ "Nothing was found for your query: '" ++ query ++ "'."

regexHelpMessage :: ChatId -> SendMessageRequest
regexHelpMessage chatId =
  sendMessageRequest chatId $
  "Use next syntax: \"/regex {i=regex;}\".\n" <>
  "i - identifier:\n" <>
  "a for album\n" <>
  "p for performer\n" <>
  "t for title\n"

tracksFoundMessage :: ChatId -> String -> SendMessageRequest
tracksFoundMessage chatId "" = sendMessageRequest chatId $ T.pack "Results for your query:"
tracksFoundMessage chatId query = sendMessageRequest chatId $ T.pack $ "Results for your query: '" ++ query ++ "':"

invalidTagsQueryMessage :: ChatId -> SendMessageRequest
invalidTagsQueryMessage chatId = sendMessageRequest chatId "Your query is either empty or invalid."

emptyCollectionMessage :: ChatId -> SendMessageRequest
emptyCollectionMessage chatId = sendMessageRequest chatId "Your collection is empty."
