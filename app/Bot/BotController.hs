{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE ViewPatterns               #-}

module Bot.BotController
  ( startApp
  ) where

import           Bot.MessageHandlers
import           Bot.Parser
import           Control.Monad            (forM_)
import           Control.Monad.Except     (MonadError, MonadIO)
import           Control.Monad.Reader     (MonadReader, ReaderT, ask, asks,
                                           liftIO, runReaderT)
import qualified Data.ByteString.Lazy     as LS (writeFile)
import           Data.List                (intercalate)
import           Data.List.Split          (splitOn)
import           Data.Maybe               (fromJust, fromMaybe)
import           Data.String.Utils        (strip)
import           Data.Text                (Text, concat, pack, stripPrefix,
                                           unpack)
import           DataManipulator
import           Network.HTTP.Client      (Manager, newManager)
import           Network.HTTP.Client.TLS  (tlsManagerSettings)
import           Network.HTTP.Simple      (getResponseBody, httpLBS,
                                           parseRequest_)
import           Network.Wai.Handler.Warp (run)
import           Servant
import           System.Environment       (getEnv)
import           Util.List                (rmdups)
import           Web.Telegram.API.Bot

newtype Bot a = Bot
  { runBot :: ReaderT BotConfig Handler a
  } deriving (Functor, Applicative, Monad, MonadIO, MonadReader BotConfig, MonadError ServantErr)

data BotConfig = BotConfig
  { telegramToken :: Token
  , manager       :: Manager
  , stringToken   :: String
  }

type BotAPI = "webhook" :> Capture "secret" Text :> ReqBody '[ JSON] Update :> Post '[ JSON] ()

botApi :: Proxy BotAPI
botApi = Proxy

startApp :: Int -> IO ()
startApp port = do
  putStrLn $ "Starting bot at port " ++ show port
  manager' <- newManager tlsManagerSettings
  telegramToken' <- getEnv "TMB_BOT"
  let config =
        BotConfig
        { telegramToken = Token $ pack $ "bot" ++ telegramToken'
        , manager = manager'
        , stringToken = "bot" ++ telegramToken'
        }
  run port $ app config

app :: BotConfig -> Application
app config = serve botApi $ initBotServer config

initBotServer :: BotConfig -> Server BotAPI
initBotServer config = enter (transform config) botServer
  where
    transform :: BotConfig -> Bot :~> Handler
    transform config = NT $ flip runReaderT config . runBot

botServer :: ServerT BotAPI Bot
botServer = handleWebhook
  where
    handleWebhook :: Text -> Update -> Bot ()
    handleWebhook secret update = do
      Token token <- asks telegramToken
      if EQ == compare secret token
        then handleUpdate update
        else error "Wrong token"

handleUpdate :: Update -> Bot ()
handleUpdate update =
  case update of
    Update {message = Just msg} -> handleMessage msg
    _ -> liftIO $ putStrLn $ "Handle update failed. " ++ show update

-- | Handles each new messages depending on it's type and attachments
handleMessage :: Message -> Bot ()
handleMessage msg = do
  BotConfig {..} <- ask
  let chatId = ChatId $ fromIntegral $ user_id $ fromJust $ from msg
      messageAudio = audio msg
      messageText = text msg
      onCommand (Just (stripPrefix "/help" -> Just _)) = sendMessageM $ helpMessage chatId
      onCommand (Just (stripPrefix "/find" -> Just _)) = handleFindMessage chatId $ fromJust messageText
      onCommand (Just (stripPrefix "/tags" -> Just _)) = handleTagsMessage chatId $ fromJust messageText
      onCommand (Just (stripPrefix "/random" -> Just _)) = handleRandomMessage chatId $ fromJust messageText
      onCommand (Just (stripPrefix "/regex" -> Just _)) = handleRegexMessage chatId $ fromJust messageText
      onCommand _ = sendMessageM (unknownCommandMessage chatId)
  case messageAudio of
    Nothing -> liftIO $ runClient (onCommand messageText) telegramToken manager
    Just audio' -> do
      let size = fromJust $ audio_file_size audio'
          fileId = audio_file_id audio'
      if size > 20 * 1024 * 1024
        then liftIO $ runClient (sendMessageM $ sizeErrorMessage chatId) telegramToken manager
        else do
          file <- liftIO $ runClient (getFileM fileId) telegramToken manager
          case file of
            Left _ -> do
              liftIO $ putStrLn "BotController.handleMessage: file download error"
              liftIO $ runClient (sendMessageM $ downloadFailedMessage chatId) telegramToken manager
            Right file' ->
              liftIO $ do
                fileContent <-
                  httpLBS
                    (parseRequest_ $
                     "https://api.telegram.org/file/" ++
                     stringToken ++ "/" ++ unpack (fromJust $ file_path (result file')))
                LS.writeFile "temp.mp3" (getResponseBody fileContent)
                trackMeta <- getTrackMeta "./temp.mp3" $ unpack (file_id (result file'))
                case trackMeta of
                  Nothing ->
                    liftIO $
                    runClient
                      (sendMessageM $ noTrackMetaMessage chatId $ fromMaybe "unknown track" (audio_title audio'))
                      telegramToken
                      manager
                  Just trackMeta' -> do
                    save trackMeta'
                    liftIO $
                      runClient
                        (sendMessageM $
                         audioAcceptedMessage chatId (title trackMeta') (album trackMeta') (performer trackMeta'))
                        telegramToken
                        manager
  return ()

-- | Handles "/find" command
handleFindMessage :: ChatId -> Text -> TelegramClient MessageResponse
handleFindMessage chatId msg = do
  let attributes = strip $ splitOn "/find" (unpack msg) !! 1
  if attributes == ""
    then sendMessageM $ emptyFindQueryMessage chatId
    else do
      let attributes' = splitOn ",," attributes
          length' = length attributes'
          name' = strip (head attributes')
          album' =
            if length' >= 2
              then strip (attributes' !! 1)
              else ""
          artist' =
            if length' >= 3
              then strip (attributes' !! 2)
              else ""
      if name' == "" && album' == "" && artist' == ""
        then sendMessageM $ emptyFindQueryMessage chatId
        else do
          let name_ =
                if name' == ""
                  then ""
                  else "\"" ++ name' ++ "\" "
              album_ =
                if album' == ""
                  then ""
                  else "from \"" ++ album' ++ "\" "
              artist_ =
                if artist' == ""
                  then ""
                  else "by " ++ artist'
              query = strip $ name_ ++ album_ ++ artist_
          tracks <- liftIO $ findByAttributes name' album' artist'
          if null tracks
            then sendMessageM $ noTracksFoundMessage chatId query
            else do
              sendMessageM $ tracksFoundMessage chatId query
              sendAudioWithCaption chatId tracks

-- | Handles "/regex" command
handleRegexMessage :: ChatId -> Text -> TelegramClient MessageResponse
handleRegexMessage chatId msg = do
  let attributes = splitOn "/regex" (unpack msg) !! 1
  if attributes == ""
    then sendMessageM $ emptyQueryMessage chatId
    else
      case parseExpr attributes of
        Left err -> sendMessageM $ regexHelpMessage chatId
        Right ex -> do
          tracks <- liftIO $ findRegex ex
          if null tracks
            then sendMessageM $ noTracksFoundMessage chatId ""
            else do
              sendMessageM $ tracksFoundMessage chatId ""
              sendAudioWithCaption chatId tracks

-- | Handles "/tags" command
handleTagsMessage :: ChatId -> Text -> TelegramClient MessageResponse
handleTagsMessage chatId msg = do
  let tags' = splitOn ",," $ splitOn "/tags" (unpack msg) !! 1
      tags_ = rmdups $ filter (/= "") $ map strip tags'
  if null tags_
    then sendMessageM $ invalidTagsQueryMessage chatId
    else do
      let query = "tags: " ++ intercalate ", " (map show tags_)
      tracks <- liftIO $ findByTags tags_
      if null tracks
        then sendMessageM $ noTracksFoundMessage chatId query
        else do
          sendMessageM $ tracksFoundMessage chatId query
          sendAudioWithCaption chatId tracks

-- | Handles "/random" command
handleRandomMessage :: ChatId -> Text -> TelegramClient MessageResponse
handleRandomMessage chatId msg = do
  tracks <- liftIO findRandom
  if null tracks
    then sendMessageM $ emptyCollectionMessage chatId
    else sendAudioWithCaption chatId tracks

-- Sends a list of track's based on their representation to chat by it's id
sendAudioWithCaption :: ChatId -> [TrackMeta] -> TelegramClient MessageResponse
sendAudioWithCaption chatId tracks = do
  forM_
    tracks
    (\a -> do
       let caption = title a ++ " - " ++ performer a
           request =
             SendAudioRequest
               chatId
               (pack $ fileId a)
               (Just $ pack caption)
               Nothing
               Nothing
               Nothing
               Nothing
               Nothing
               Nothing
       sendAudioM request)
  sendMessageM $ sendMessageRequest chatId ""
