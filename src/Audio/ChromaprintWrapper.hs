module Audio.ChromaprintWrapper
  ( Chromaprint(..)
  , getChromaprint
  ) where

import           Data.List.Split (splitOn)
import           System.Exit     (ExitCode (ExitFailure, ExitSuccess))
import           System.Process  (readProcessWithExitCode)

-- | Encapsulation of the recordings's duration and fingerprint
data Chromaprint = Chromaprint
  { duration    :: Int
  , fingerprint :: String
  } deriving (Show)

-- | Returns recordings's duration and fingerprint
getChromaprint :: String -> IO Chromaprint
getChromaprint path = do
  (errCode, stdout, stderr) <- readProcessWithExitCode "fpcalc" [path] []
  case errCode of
    ExitSuccess -> do
      let fcalcOut = lines stdout
      return
        Chromaprint
        {duration = read $ splitOn "=" (fcalcOut !! 0) !! 1 :: Int, fingerprint = splitOn "=" (fcalcOut !! 1) !! 1}
    ExitFailure code -> error $ "ChromaprintWrapper.getChromaprint: fpaclc finished with error code " ++ show code
