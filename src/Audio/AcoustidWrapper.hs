{-# LANGUAGE OverloadedStrings #-}

module Audio.AcoustidWrapper
  ( MusicBrainzMeta(..)
  , getTrackData
  ) where

import           Audio.ChromaprintWrapper
import           Control.Monad             (mzero)
import           Data.Aeson                (FromJSON (..), Value (..), decode,
                                            (.:), (.:?))
import           Data.List                 (intercalate)
import           Data.Maybe                (fromJust, isNothing)
import           Network.HTTP.Simple       (getResponseBody, getResponseStatus,
                                            httpLBS, parseRequest_)
import           Network.HTTP.Types.Status (statusCode)
import           System.Environment        (getEnv)
import qualified Util.QueryString          as QS

-- | Representation of recording's information from MsicBrainz database
data MusicBrainzMeta = MusicBrainzMeta
  { trackName      :: String
  , trackAlbum     :: String
  , trackPerformer :: String
  , musicBrainzId  :: String
  } deriving (Show)

data Resp = Resp
  { respStatus  :: String
  , respResults :: [Result]
  } deriving (Show)

data Result = Result
  { resultScore      :: Double
  , resultId         :: String
  , resultRecordings :: Maybe [Recording]
  } deriving (Show)

data Recording = Recording
  { recordingSources       :: Int
  , recordingDuration      :: Maybe Int
  , recordingTitle         :: Maybe String
  , recordingId            :: String
  , recordingReleaseGroups :: Maybe [ReleaseGroup]
  , recordingArtists       :: Maybe [Artist]
  } deriving (Show)

data Artist = Artist
  { artistName :: String
  , artistId   :: String
  } deriving (Show)

data ReleaseGroup = ReleaseGroup
  { releaseGroupSecondaryTypes :: Maybe [String]
  , releaseGroupArtists        :: Maybe [Artist]
  , releaseGroupId             :: String
  , releaseGroupTitle          :: String
  , releaseGroupType           :: Maybe String
  } deriving (Show)

instance FromJSON Resp where
  parseJSON (Object v) = Resp <$> v .: "status" <*> v .: "results"
  parseJSON _          = mzero

instance FromJSON Result where
  parseJSON (Object v) = Result <$> v .: "score" <*> v .: "id" <*> v .:? "recordings"
  parseJSON _ = mzero

instance FromJSON Recording where
  parseJSON (Object v) =
    Recording <$> v .: "sources" <*> v .:? "duration" <*> v .:? "title" <*> v .: "id" <*> v .:? "releasegroups" <*>
    v .:? "artists"
  parseJSON _ = mzero

instance FromJSON Artist where
  parseJSON (Object v) = Artist <$> v .: "name" <*> v .: "id"
  parseJSON _          = mzero

instance FromJSON ReleaseGroup where
  parseJSON (Object v) =
    ReleaseGroup <$> v .:? "secondarytypes" <*> v .:? "artists" <*> v .: "id" <*> v .: "title" <*> v .:? "type"
  parseJSON _ = mzero

instance Ord Result where
  compare a b = compare (resultScore a) (resultScore b)

instance Eq Result where
  a == b = resultScore a == resultScore b

instance Ord Recording where
  compare a b = compare (recordingSources a) (recordingSources b)

instance Eq Recording where
  a == b = recordingSources a == recordingSources b

meta = ["recordings", "releasegroups", "compress", "sources"]

-- | Returns recording's information associated with fingerprint and duration
-- If there is no such information returns Nothing
getTrackData :: Chromaprint -> IO (Maybe MusicBrainzMeta)
getTrackData acoustidData = do
  token <- getEnv "ACOUSTID_TOKEN"
  let query =
        [ ("client", token)
        , ("duration", show (duration acoustidData))
        , ("fingerprint", fingerprint acoustidData)
        , ("meta", intercalate "+" meta)
        ]
  response <- httpLBS $ parseRequest_ $ "https://api.acoustid.org/v2/lookup?" ++ QS.pack query
  let code = statusCode $ getResponseStatus response
  if code == 200
    then case decode $ getResponseBody response :: Maybe Resp of
           Nothing -> error "AcoustidWrapper.getTrackData: error parsing response"
           Just resp ->
             if null $ respResults resp
               then return Nothing
               else case resultRecordings $ maximum $ respResults resp of
                      Just recordings -> do
                        let recording = maximum recordings
                        return
                          (Just
                             MusicBrainzMeta
                             { trackName = fromJust $ recordingTitle recording
                             , trackAlbum =
                                 case recordingReleaseGroups recording of
                                   Just releaseGroups -> do
                                     let primaryReleases =
                                           filter (\a -> isNothing $ releaseGroupSecondaryTypes a) releaseGroups
                                     if null primaryReleases
                                       then releaseGroupTitle $ head releaseGroups
                                       else releaseGroupTitle $ head primaryReleases
                                   Nothing -> ""
                             , trackPerformer = artistName $ head $ fromJust $ recordingArtists recording
                             , musicBrainzId = recordingId recording
                             })
                      Nothing -> return Nothing
    else error ("AcoustidWrapper.getTrackData: Acoustid responded with error code " ++ show code)
