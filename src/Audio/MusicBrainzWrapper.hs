{-# LANGUAGE OverloadedStrings #-}

module Audio.MusicBrainzWrapper
  ( getTags
  ) where

import           Control.Monad             (mzero)
import           Data.Aeson                (FromJSON (..), Value (..), decode,
                                            (.:))
import           Network.HTTP.Simple       (getResponseBody, getResponseStatus,
                                            httpLBS, parseRequest_,
                                            setRequestHeaders)
import           Network.HTTP.Types.Status (statusCode)
import qualified Util.QueryString          as QS

data Resp = Resp
  { respTags           :: [Tag]
  , respVideo          :: Bool
  , respDisambiguation :: String
  , respTitle          :: String
  , respLength         :: Int
  , respId             :: String
  } deriving (Show)

data Tag = Tag
  { tagName  :: String
  , tagCount :: Int
  } deriving (Show)

instance FromJSON Resp where
  parseJSON (Object v) =
    Resp <$> v .: "tags" <*> v .: "video" <*> v .: "disambiguation" <*> v .: "title" <*> v .: "length" <*> v .: "id"
  parseJSON _ = mzero

instance FromJSON Tag where
  parseJSON (Object v) = Tag <$> v .: "name" <*> v .: "count"
  parseJSON _          = mzero

headers = [("User-Agent", "The Musical Box/0.0.1 ( ishulgin11@gmail.com )")]

query = [("inc", "tags"), ("fmt", "json")]

-- | Returns a list of tags associated with the recording's id on musicbrainz.com
getTags :: String -> IO [String]
getTags id = do
  let request =
        setRequestHeaders headers $
        parseRequest_ $ "http://musicbrainz.org/ws/2/recording/" ++ id ++ "?" ++ QS.pack query
  response <- httpLBS request
  let code = statusCode $ getResponseStatus response
  if code == 200
    then case decode $ getResponseBody response :: Maybe Resp of
           Nothing -> error "MusicBrainzWrapper.getTags: error parsing response"
           Just resp -> return $ map tagName $ filter (\a -> tagCount a > 0) $ respTags resp
    else error $ "MusicBrainzWrapper.getTags: MusicBrainz responded with error code " ++ show code
