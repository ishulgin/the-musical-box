module Util.QueryString
  ( pack
  ) where

import           Data.List (intercalate)

pack :: [(String, String)] -> String
pack input = intercalate "&" $ map (\(a, b) -> intercalate "=" [a, b]) input
