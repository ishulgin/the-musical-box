module Util.List
  ( rmdups
  ) where

import           Data.List

rmdups :: (Ord a) => [a] -> [a]
rmdups = map head . group . sort
